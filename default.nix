{ pkgs ? import <nixpkgs> { } }:

rec {
  dsc = pkgs.callPackage ./pkgs/dsc {  };
  dsc-gateway = pkgs.callPackage ./pkgs/dsc-gateway { };

  dsv = pkgs.callPackage ./pkgs/dsv { };
  dsv-edit = dsv.override {
    enableEdit = true;
  };
  #dsv-edit = pkgs.callPackage ./pkgs/dsv { enableEdit = true; };
  
  dsm = pkgs.callPackage ./pkgs/dsm {  };

  #nixos = import ./nixos;

  path = ./.;
}
