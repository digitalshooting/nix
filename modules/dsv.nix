{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.dsv;
  dsv = pkgs.callPackage ../pkgs/dsv { enableEdit = cfg.canEdit; };
  name = "dsv";
in
{
  options.services.dsv = {
    enable = mkEnableOption "DSV";

    groups = mkOption {
      type = types.listOf types.str;
      default = [];
      description = ''
        Groups to which the user ${name} should be added.
      '';
    };

    workDir = mkOption {
      type = types.path;
      default = "/var/lib/${name}";
      description = ''
        Working directory for the DSM service.
      '';
    };

    canEdit = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Can edit
      '';
    };

    package = mkOption {
      type = types.package;
      default = dsv;
      description = ''
        Package that provides DSM.
      '';
    };
  };

  config = mkIf cfg.enable {
    services.nginx.enable = true;
    services.nginx.virtualHosts = {
      "0.0.0.0" = {
        root = "${cfg.package}/";
        locations."/socket.io/".proxyPass = "http://localhost:4000";
      };
    };

    networking.firewall.allowedTCPPorts = [ 80 ];
  };
}
