{ config, pkgs, lib, ... }:

{
  imports = [
    ./dsc.nix
    ./dsc-gateway.nix
    ./dsm.nix
    ./dsv.nix
  ];
}
